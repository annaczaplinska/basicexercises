package zadanie29;

import java.util.Scanner;

public class MainZadanie29 {
    public static void main(String[] args) {

        int liczbaRazy=0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków:");
        String ciagZnaczkow = scanner.nextLine();

        System.out.println("Twój ciąg znaków: ");

        System.out.println(ciagZnaczkow);
        char ostatniZnak = ciagZnaczkow.charAt(ciagZnaczkow.length()-1);

        for (int i=0 ; i< ciagZnaczkow.length() ; i++){
            if (ciagZnaczkow.charAt(i) == ostatniZnak){
                liczbaRazy += 1;
            }
        }
        System.out.println(liczbaRazy + " razy powstarza się ostatni wyraz czyli: " + ostatniZnak);

    }
}
