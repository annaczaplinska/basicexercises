package zadanie19;

import java.util.Scanner;

public class MainZadanie19 {
    public static void main(String[] args) {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int suma=0;

        Scanner scanner = new Scanner(System.in);
        int liczba = 0;

        do{
            System.out.println("Podaj liczbe");
            int liczba2 = scanner.nextInt();
            if(liczba2 == 0){
                break;
            }

            if (max<liczba2){
                liczba=liczba2;
                max=liczba;
            }

            if (min>liczba2){
                liczba=liczba2;
                min=liczba;
            }
            suma+=liczba;
        }while (liczba != 0);
        double sumaminimax = min + max;
        System.out.println("Suma max i min liczby: " + sumaminimax);
        double srednia = sumaminimax/2;
        System.out.println("Srednia z min i max liczby: " + srednia);


    }
}
