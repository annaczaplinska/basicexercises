package zadanie36;

import java.util.Scanner;

public class MainZadanie36 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Jestem programme ktory liczby pierwiastki rownania kwadratowego");
        System.out.println("Podaj wartosc A:");
        int A = scanner.nextInt();
        System.out.println("Podaj wartosc B:");
        int B = scanner.nextInt();
        System.out.println("Podaj wartosc C");
        int C = scanner.nextInt();

        double delta = B*B+4*A*C;
        if (delta == 0){
            System.out.println("delta= " + delta);
            System.out.println("xo= " + -B/(2*A));
        } else if (delta>0){
            System.out.println("delta= " + delta);
            System.out.println("x1= " + (-B - Math.sqrt(delta))/(2*A));
            System.out.println("x2= " + (-B + Math.sqrt(delta))/(2*A));
        }else{
            System.out.println("delta= " + delta);
            System.out.println("Równanie nie ma pierwiastków");
        }
    }
}
