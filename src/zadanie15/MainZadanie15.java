package zadanie15;

import java.util.Scanner;

public class MainZadanie15 {
    public static void main(String[] args) {

        System.out.println("Jestem kalkulator. Dzięki mnie możesz dodawać, odejmować i dzielić!!! :) ");
        Scanner kalkulator = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę");
        double liczba1 = kalkulator.nextInt();

        System.out.println("Podaj symbol operacji matematycznej. Dostepne: + , - , * , /");
        String symbol = kalkulator.next();
        if (symbol.equals("+") || symbol.equals("-") || symbol.equals("*") || symbol.equals("/")){
            System.out.println("Podaj druga liczbę");
            double liczba2 = kalkulator.nextInt();

            if (symbol.equals("+")){
                double wynik = liczba1 + liczba2;
                System.out.println(liczba1 + " + " + liczba2 + " = " + "Wynik: " + wynik);
            } else if (symbol.equals("-")){
                double wynik = liczba1 - liczba2;
                System.out.println(liczba1 + " - " + liczba2 + " = " + "Wynik: " + wynik);
            } else if (symbol.equals("*")){
                double wynik = liczba1 * liczba2;
                System.out.println(liczba1 + " * " + liczba2 + " = " + "Wynik: " + wynik);
            }else if (symbol.equals("/")){
                double wynik = liczba1/liczba2;
                System.out.println(liczba1 + " / " + liczba2 + " = " + "Wynik: " + wynik);
            }
        }else System.out.println("Podałeś niepoprawny znak!!");



    }
}
