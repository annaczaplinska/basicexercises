package zadanie30;

import java.util.Scanner;

public class MainZadanie30 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków");
        String ciagZnakow = scanner.nextLine();

        System.out.println("Twój ciag znaków pisany od tyłu: \n ");
        for (int i = ciagZnakow.length() - 1; i >= 0; i--) {
            System.out.print(ciagZnakow.charAt(i));
        }

    }
}
