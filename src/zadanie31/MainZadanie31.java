package zadanie31;

import java.util.Scanner;

public class MainZadanie31 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków. A ja sprawdzę czy jest on palindromem :) ");
        String czyPalindrom = scanner.nextLine();

        boolean palindrom = true;

        for (int i = 0; i < czyPalindrom.length() - 1; i++) {
//            int j = czyPalindrom.length()-1-i;
//            if (czyPalindrom.charAt(i) != czyPalindrom.charAt(j)) {
//                palindrom = false;
//            }
            for (int j = czyPalindrom.length() - 1-i; j >= czyPalindrom.length() - 1 - i; j--) {
                System.out.println("Porównuje znak: " + czyPalindrom.charAt(i) + " ze znakiem: " + czyPalindrom.charAt(j));
                if (czyPalindrom.charAt(i) != czyPalindrom.charAt(j)) {
                    palindrom = false;
                }

            }
            System.out.println();
        }

        if (palindrom) {
            System.out.println("Jest palindromem :)");
        } else {
            System.out.println("Nie jest palindromem :(");
        }
    }
}
