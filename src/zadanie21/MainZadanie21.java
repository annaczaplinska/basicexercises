package zadanie21;

import java.util.Scanner;

public class MainZadanie21 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilość rzędów choinki :)");
        int rzedy = scanner.nextInt();

        while (rzedy < 0) {
            System.out.println("Podałeś ujemna ilość rzędów. Podaj poprawną ilość rzędów choinki: ");
            int rzedy2 = scanner.nextInt();
            rzedy = rzedy2;
        }

        int ile_spacji = rzedy - 1;

        int ile_gwiazdek = 1;

        for (int poziom = 0; poziom < rzedy; poziom++) {

            for (int spacja = 0; spacja < ile_spacji; spacja++) {

                System.out.print(" ");

            }

            for (int gwiazdka = 0; gwiazdka < ile_gwiazdek; gwiazdka++) {

                System.out.print("*");

            }

            for (int spacja = 0; spacja < ile_spacji; spacja++) {

                System.out.print(" ");

            }

            ile_spacji--;

            ile_gwiazdek += 2;

            System.out.println();

        }
    }
}
