package zadanie25;

public class MainZadanie25 {
    public static void main(String[] args) {

        int[] tablica = new int[10];
        int wystapienia = 0;


        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = (int) ((Math.random() * 10) + 1);
        }

        System.out.println("Wylosowane liczby: ");
        //wypisanie elementów tablicy

        for (int n = 0; n < tablica.length; n++) {
            System.out.print(tablica[n] + " ");
        }

        System.out.println();
        System.out.println("Wystapienia:");

        // ile razy wystepuje dana liczba
        for (int x = 0; x <= 10; x++) {
            for (int i = 0; i < tablica.length; i++) {
                if (tablica[i] == x) {
                    wystapienia += 1;
                }
            }
            System.out.println(x + " - " + wystapienia);
            wystapienia=0;
        }
    }

}