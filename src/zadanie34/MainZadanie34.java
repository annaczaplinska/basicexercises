package zadanie34;

import java.util.Scanner;

public class MainZadanie34 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj tresc do zaszyfrowania szyfrem Cezara:");
        String podaje = scanner.nextLine();
        System.out.println("Podaj przesuniecie:");
        int przesuniecie = scanner.nextInt();

        //zamiieniam ze znakow na asci, przesuwam o "przesuniecie" i z ascii na zwykle i je wyswietlam

        for (int i =0; i < podaje.length() ; i++){

            int kod_asci = podaje.charAt(i) + przesuniecie;

            char wyswietl = (char) (kod_asci);

            System.out.print(wyswietl);
        }

    }
}
