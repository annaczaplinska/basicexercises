package zadanie28;

import java.util.Scanner;

public class MainZadanie28 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dodatnia liczbę całkowitą");
        int liczba = scanner.nextInt();

        while (liczba <= 0) {
            System.out.println("Podałes liczbę: " + liczba + " Podaj dodatnią liczbe całkowitą");
            int liczba2 = scanner.nextInt();
            liczba = liczba2;
        }
        System.out.println("Super. Podałeś liczbę: " + liczba);

        boolean[][] tablica = new boolean[liczba+2][liczba+2];

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {
                tablica[i][j] = true;
            }
        }


        for (int i = 2; i < liczba+2; i++) {
            for (int j = 2; j < liczba+2; j++) {
                int a;

                if (i > j) {
                    a = j;
                } else {
                    a = i;
                }
                for (int k = 2; k < a; k++) {
                    if ((((i + 1) % k )== 0) && (((j + 1) % k) == 0)) {
                        tablica[i][j]=false;
                    }
                }
            }
        }
        for (int i = 2; i < liczba+2; i++) {
            for (int j = 2; j < liczba+2; j++) {
                if(tablica[i][j]){
                    System.out.print("+ ");
                }else{
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}