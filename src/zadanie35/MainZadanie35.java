package zadanie35;

import java.util.Scanner;

public class MainZadanie35 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Jestem programem do dzielenia liczb. Podam Ci wynik dzialania a/b");
        System.out.println("Podaj liczbe a");
        int a = scanner.nextInt();
        System.out.println("Podaj liczbe b");
        int b = scanner.nextInt();
        while (true) {
            try {
                if(b < 0){
                    throw new IllegalArgumentException();
                }
                System.out.println(a + "/" + b + "=" + (double) a / b);
                break;
            } catch (IllegalArgumentException iae) {
                System.out.println("b jest liczbą ujemną..");
                System.out.println("Podaj nową liczbe b");
                b = scanner.nextInt();
            }
        }



    }
}
