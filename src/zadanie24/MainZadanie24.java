package zadanie24;

public class MainZadanie24 {
    public static void main(String[] args) {

        int [] tablica = new int[10];
        double sum=0;
        int wieksze =0;
        int mniejsze =0;

        for (int i=0 ; i<tablica.length ; i++){
            tablica[i]= (int)((Math.random()*20)-10);
        }
        System.out.println("Wylosowane liczby:");

        // wyświetlenie elementów tablicy
        for (int i=0; i<tablica.length; i++){
            System.out.print(" " + tablica[i]);
            sum+=tablica[i];
        }

        System.out.println();
        double srednia = sum/tablica.length;
        System.out.println("średnia arytmetyczna: " + srednia);

        // najmniejszy element tablicy
        int wynik = tablica[0];
        for (int i=0 ; i<tablica.length; i++){
            if (wynik>tablica[i]){
                    wynik=tablica[i];
            }
        }

        System.out.println("Najmniejszy element: " + wynik);

        // najwięksszy element tablicy
        wynik = tablica[0];
        for (int i =0; i<tablica.length; i++){
            if (wynik< tablica[i]){
                wynik=tablica[i];
            }
        }
        System.out.println("Największy element: " + wynik);

        //elementy mniejsze od sredniej
        System.out.println("Ilośc elementów mniejszych od średniej: ");
        for (int i=0; i<tablica.length; i++){
            if (srednia>tablica[i]){
                mniejsze += 1;
            }
        }
        System.out.println(mniejsze);

        //elementy wieksze od sredniej
        System.out.println("Ilośc elementów większych od średniej: ");
        for (int i=0; i<tablica.length; i++){
            if (srednia<tablica[i]){
                wieksze += 1;
            }
        }
        System.out.print(wieksze);

        // zawartosc tablicy w odwrotnej kolejnosci
        System.out.println();
        System.out.println("Zawartość tablicy w odwrotnej kolejnośći: ");
        for (int i = tablica.length-1 ; i>=0 ; i--){
            System.out.print(" " + tablica[i]);
        }

    }
}
