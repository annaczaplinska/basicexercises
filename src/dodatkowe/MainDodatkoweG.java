package dodatkowe;

import java.util.Scanner;

public class MainDodatkoweG {
    public static void main(String[] args) {

        int liczbaRazy=0;

        System.out.println("Jestem programem który zlicza ilość wystąpień danego znaku w ciągu znaków");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków:");
        String ciagZnaczkow = scanner.nextLine();
        System.out.println("Jaki znak mam sprawdzić?");
        char znak = scanner.next().charAt(0); // 1 litera slowa

        System.out.println("Twój ciąg znaków: ");

        System.out.println(ciagZnaczkow);

        System.out.println("szukamy: "+znak);


        for (int i=0 ; i< ciagZnaczkow.length() ; i++){
            if (ciagZnaczkow.charAt(i) == znak){
                liczbaRazy += 1;
            }
        }
        System.out.println(liczbaRazy + " razy powstarza się: " + znak);

    }
}
