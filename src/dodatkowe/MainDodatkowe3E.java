package dodatkowe;

import java.util.Scanner;

public class MainDodatkowe3E {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj promień koła: ");
        double r = (int)scanner.nextInt();

        double srednia = 2 * r;

        for (int i = 0; i < srednia; i++) {
            for (int j = 0; j < srednia; j++) {
                double a = i - r;
                double b = j - r;
                if (((a * a) + (b * b))< (r * r)) {
                    System.out.print(" .");
                }else{
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
    }
}
