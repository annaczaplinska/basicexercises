package dodatkowe;

import java.util.Scanner;

public class MainDodatkowe3C {

    public static void main(String[] args) {

        System.out.println("Jestem programem ktory rysuje pusty prostokat :)");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilość wierszy: ");
        int n = scanner.nextInt();
        System.out.println("Podaj ilosc kolumn: ");
        int m = scanner.nextInt();


        for (int i = 0; i < n; i++) {
            if (i == 0 || (i == n - 1)) {
                for (int j = 0; j < m; j++) {
                    System.out.print("*");
                }
            }else {
                for (int j = 0; j < m; j++) {
                    if (j == 0 || (j == m - 1)) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
