package zadanie6;

public class MainZadanie6 {
    public static void main(String[] args) {

        int ocena_matematyka=5;
        int ocena_chemia=4;
        int ocena_j_polski=3;
        int ocena_j_angielski=1;
        int ocena_wos=4;
        int ocena_informatyka=4;

        double srednia_ocen = (ocena_matematyka + ocena_chemia + ocena_j_polski + ocena_j_angielski + ocena_wos + ocena_informatyka)/6.0;
        double srednia_scislych= (ocena_matematyka + ocena_chemia + ocena_informatyka)/3.0;
        double srednia_human= (ocena_j_polski + ocena_j_angielski + ocena_wos)/3.0;

        if (ocena_matematyka == 1){
            System.out.println("Ocena z matematyki jets niedostateczna !!!");
        } else if (ocena_chemia == 1){
            System.out.println("Ocena z chemii jest niedostateczna!!!");
        }else if (ocena_j_polski ==1){
            System.out.println("Ocena z j. polskiego jest niedostateczna !!!");
        }else if (ocena_j_angielski ==1){
            System.out.println("Ocena z j. angielskiego jest niedostateczna!!!");
        }else if (ocena_wos ==1){
            System.out.println("Ocena z wos'u jest niedostateczna!!!");
        }else if (ocena_informatyka == 1){
            System.out.println("Ocena z informatyki jest niedostateczna !!!");
        }


        System.out.println("średnia wszystkich ocena:");
        System.out.printf("%.2f",srednia_ocen);
        System.out.println();
        System.out.println("średnia ocen z przedmiotów ścisłych:");
        System.out.printf("%.2f",srednia_scislych);
        System.out.println();
        System.out.println("średnia ocen z przedmiotów humanistycznych");
        System.out.printf("%.2f",srednia_human);
    }
}
