package zadanie33;

import java.util.Scanner;

public class MainZadanie33 {
    public static void main(String[] args) {

        int otwarte = 0;
        int zamkniete = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj równanie. Sprawdzę czy dobrze sparowałeś nawiasy :)");
        String podaje = scanner.nextLine();
        boolean parowanie = true;


        for (int i = 0; i < podaje.length(); i++) {
            if (podaje.charAt(i) == '(') {
                otwarte++;
            } else if (podaje.charAt(i) == ')') {
                zamkniete++;
            }
            while (zamkniete > otwarte) {
                parowanie = false;
                break;
            }
        }

        if (zamkniete != otwarte){
            parowanie = false;
        }
        if(parowanie){
            System.out.println("OK. Nawiasiki sa w porządku :)");
        } else {
            System.out.println("Coś nie tak.. błędne spraowanie nawiasów !!");
        }


    }
}
