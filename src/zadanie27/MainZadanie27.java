package zadanie27;

import java.util.Scanner;

public class MainZadanie27 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wyświetlę dla Ciebie tabliczke mnożenia. Do jakiej liczby sobie życzysz?");
        int N = scanner.nextInt();

        //tabliczka mnożenia do N*N
        for (int i =1; i<=N ; i++){
            for (int j =1; j<=N ; j++ ){
                int mnozenie = i*j;
                System.out.print(i + "*" + j + "=" + mnozenie + "     ");
            }
            System.out.println();
        }

    }
}
