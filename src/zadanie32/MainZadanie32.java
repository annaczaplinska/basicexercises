package zadanie32;

import java.util.Scanner;

public class MainZadanie32 {
    public static void main(String[] args) {

        int suma=0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wypisz jakis tekst: ");
        String ciagZnakow = scanner.nextLine();

// petla przez wszystkie znaki jezeli cyfra dodaje wartosc do sumy

        for (int i=0; i< ciagZnakow.length() ; i++){

            int kod_asci = ciagZnakow.charAt(i);
            if(kod_asci>='0' && kod_asci<='9'){
                suma += kod_asci-'0';
            }
        }
        System.out.println("Suma cyfr w podanym przez Ciebie zdaniu wynosi: " + suma);
    }
}
