package zadanie20;

import java.util.Random;
import java.util.Scanner;

public class MainZadanie20 {
    public static void main(String[] args) {

        int random = (int) ((Math.random() * 99) + 1);
        Random generator = new Random();
//        int random = generator.nextInt(99) + 1;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Odgadnij liczbe!");
        int zgaduje = scanner.nextInt();

        while (zgaduje != random) {

            if (zgaduje < random) {
                System.out.println("Za mało!!");
            }
            if (zgaduje > random) {
                System.out.println("Za dużo");
            }
            System.out.println("Odgadnij liczbę");
            int zgaduje2 =scanner.nextInt();
            zgaduje = zgaduje2;

        }
        if (zgaduje == random) {
            System.out.println("Gratulacje!! Odgadłeś liczbę!! Liczba: " + random);
        }
    }
}
