package zadanie17;

import java.util.Scanner;

public class MainZadanie17 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int n = scanner.nextInt();
        if (n<0){
            System.out.println("Podałeś liczbę ujemną!");
        }

//                a następnie wyświetla na ekranie wszystkie potęgi liczby 2 nie większe, niż podana liczba.
//        Przykładowo, dla liczby 71 program powinien wyświetlić:
//        1 2 4 8 16 32 64


        for (int i=0; i<=n ; i++) {
            double wynik = Math.pow(2.0 , i);
            if (wynik < n) {
                System.out.println(wynik);
            }
        }


    }
}
